#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $s = join("", (0..1000000));
say substr($s, 1, 1)
	*substr($s, 10, 1)
	*substr($s, 100, 1)
	*substr($s, 1000, 1)
	*substr($s, 10000, 1)
	*substr($s, 100000, 1)
	*substr($s, 1000000, 1);