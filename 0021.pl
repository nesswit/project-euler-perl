#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my %h;
my $r = 0;
for (1..10000) {
	my $t = 0;
	$t += $_ for (all_factors($_));
	$h{$_} = $t + 1;
}

for (keys %h) {
	$r += $_ if ($_ != $h{$_} and defined $h{$h{$_}} and $_ == $h{$h{$_}});
}

say $r;
