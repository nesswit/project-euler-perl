#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use bigint;
use Math::Prime::Util ':all';

my $c = 0;
my $r = 0;
for (1..1000) {
	$_ /= 2 while ($_ % 2 == 0);
	$_ /= 5 while ($_ % 5 == 0);
	for my $i (1..$_-1) {
		if (10**$i % $_ == 1) {
			if ($i > $c) {
				$c = $i;
				$r = $_;
			}
			last;
		}
	}
}

say $r;