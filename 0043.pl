#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Permute::Array;

my $r = 0;
my @a = (0..9);
my @p = (2, 3, 5, 7, 11, 13, 17);
my $p = new Math::Permute::Array(\@a);
for my $i (0..$p->cardinal()-1) {
	my @t = @{$p->permutation($i)};
	next if ($t[0] == 0 or $t[3] % 2 == 1 or ($t[5] != 0 and $t[5] != 5));
	my $b = 1;
	for my $j (0..6) {
		my $k = $t[$j+1]*100+$t[$j+2]*10+$t[$j+3];
		$b = 0 if ($k % $p[$j] != 0);
	}
	$r += join("", @t) if ($b);
}

say $r;