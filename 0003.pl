#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Complex;

my $a = 0;
my $b = 600851475143;
my $sqrtb = int(sqrt(600851475143));
foreach (2..$sqrtb) {
	while ($b%$_ == 0) {
		$b /= $_;
		$a = $_ ;
	}
}

say $a;