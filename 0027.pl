#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my $c = 0;
my $r;
for my $i (-998..998) {
	for my $j (3..999) {
		for (my $n = 0; is_prime($n*$n + $i*$n +$j); $n++) {
			if ($n > $c) {
				$c = $n;
				$r = $i * $j;	
			} 
		}
	}
}

say $r;