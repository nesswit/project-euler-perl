#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Complex;

my $c = 285;
while($c++) {
    my $n = $c*($c+1)/2;
    if (((1+sqrt(1+24*$n))/6) =~ m/^\d+$/ and ((1+sqrt(1+8*$n))/4) =~ m/^\d+$/){
        say $n;
        exit;
    }
}