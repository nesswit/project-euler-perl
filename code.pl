#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my %h;
my $p = 1;
my @a = ([], [], [], []);
while ($p = next_prime($p)) {
    @{$h{$p}} = [];
    for (keys %h) {
        next if ($_ == $p);
        if (is_prime($_.$p) and is_prime($p.$_)) {
            push @{$h{$_}}, $p;
            push @{$h{$p}}, $_;
            push @{$a[0]}, [$p, $_];
        }
    }
    
    for my $i (2, 1, 0) {
        my @t = @{$a[$i]};
        for (@t) {
            my $b = 1;
            for (@{$_}) {
                $b = 0 if (!c($_, @{$h{$p}}));
            }
            if ($b == 1) {
                push @{$a[$i+1]}, [@{$_}, $p];
            }
        }
    }
    
    if ($#{$a[3]} == 0) {
        my $k = 0;
        $k += $_ for (@{$a[3][0]});
        say $k;
        exit;
    }
}

sub c {
    my $s = shift;
    my %p = map { $_ => 1 } @_;
    return exists($p{$s});
}