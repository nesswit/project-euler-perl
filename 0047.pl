#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my $n = 0;
while (++$n) {
    if (f($n) == 4 and f($n+1) == 4 and f($n+2) == 4 and f($n+3) == 4) {
        say $n;
        exit;
    }
}

my %h;

sub f {
    my $n = shift;
    return $h{$n} if defined $h{$n};
        
    my ($i, %s, @r);
    foreach my $i (factor($n)) {
        push(@r, $i) unless $s{$i}++;
    }
    $h{$n} = $#r+1;
    return $#r+1;
}