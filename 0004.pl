#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $max = 0;
foreach my $i (100..999) {
	foreach my $j (100...999) {
		my $temp = $i * $j;
		if (($temp == reverse $temp) and $temp > $max) {
			$max = $temp;
		}
	}
}

say $max;