#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use bigint;

my $i = 1;
my $r = 0;
$i *= $_ for (1..100);
$r += $_ for (split(//, $i));

say $r;