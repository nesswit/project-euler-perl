#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

for my $i (1..1000) {
	for my $j (1..$i) {
		next if ($i+$j >= 1000);
		my $k = 1000 - ($i+$j);
		if ($i**2 == $j**2 + $k**2 and $i+$j+$k == 1000){
			say $i*$j*$k;
			exit;
		}
	}
}