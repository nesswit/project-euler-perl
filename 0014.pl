#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $r = 0;
my $c = 0;
for (1..1000000) {
	my $t = 0;
	my $k = $_;
	while ($k > 1) {
		$t++;
		if ($k%2 == 0) {
			$k /= 2;
		} else {
			$k = 3*$k +1; 
		}
	}
	if ($t > $c) {
		$c = $t;
		$r = $_;
	}
}

say $r;