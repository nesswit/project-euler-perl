#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $r = 0;
my $l = 9;
my @f;
push @f, f($_) for (0..9); 
$l .= 9 while ($l < length($l)*($f[9]));

for my $i (3..$l) {
	my $a = 0;
	$a += $f[$_] for (split (//, $i));
	$r += $i if ($i == $a);
}

say $r;

sub f {
	my $r = 1;
	$r *= $_ for (1..$_[0]);
	return $r;
}