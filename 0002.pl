#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $a = 1;
my $b = 0;
my $sum = 0;
while ($a <= 4000000) {
	$sum += $a if ($a%2 == 0);
	my $temp = $a + $b;
	$b = $a;
	$a = $temp;
}

say $sum;