#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use DateTime;

my $dt = DateTime->new(
	year => 1901,
	month => 1,
	day => 1
);
my $dr = DateTime::Duration->new(
    months => 1
);
my $r = 0;

while ($dt->year < 2001) {
	$r++ if ($dt->day_of_week == 7);
	$dt->add_duration($dr);
}

say $r;