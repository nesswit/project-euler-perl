#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $r = 0;
for (1..1000000) {
	if ($_ == reverse $_) {
		my $b = sprintf ("%b", $_);
		$r += $_ if ($b == reverse $b);
	}
}

say $r;