#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::BigInt;

my $n = Math::BigInt->bzero(); 
for (1..1000) {
    my $t = Math::BigInt->new($_);
    $t->bpow($t);
    $n->badd($t);
}

say substr($n->bstr(), -10);