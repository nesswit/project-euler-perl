#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my $r = 1;
my @p = @{primes( 20 )};
foreach (@p) {
	for (my $i = 1; $_**$i <= 20; $i++) {
		$r *= $_;
	}
}

say $r;