#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my @a;
for (@{primes(1000000)}) {
	my @t = split (//, $_);
	my $b = 1;
	for (0..length($_)-2) {
		push (@t, shift(@t));
		next if ($t[0] == 0);
		my $s = join("", @t);
		if (!is_prime($s)) {
			$b = 0;
		}
	}
	push (@a, join("", @t)) if ($b);
}

say $#a+1;