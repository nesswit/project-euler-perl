#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my $c = 11;
my $r = 0;
my @a;
my $p = 7;
while (1) {
	$p = next_prime($p);
	my @a = my @b = split (//, $p);
	my $b = 1;
	for (0..length($p)-2) {
		shift(@a);
		pop(@b);
		if (!is_prime(join("", @a)) or !is_prime(join("", @b))) {
			$b = 0;
		}
	}
	if ($b) {
		$r += $p;
		last if (--$c == 0);
	}
}

say $r;