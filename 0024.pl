#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $f = &f(10);
my @a = (0..9);
my $b = 0;
my $r = "";
$r .= g($r) for (0..9);

say $r;

sub g {
	for my $i (0..$#a+1) {
		if (($b + &f($#a+1)*$i / ($#a+1)) <= 1000000 and $b + &f($#a+1)*($i+1) / ($#a+1) >= 1000000) {
			$b = $b + &f($#a+1)*$i / ($#a+1);
			my $r = $a[$i];
			splice (@a, $i, 1); 
			return $r;
		}
	}
}

sub f {
	my $r = 1;
	for my $i (1..$_[0]) {
		$r *= $i;
	}
	return $r;
}