#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Complex;


my $c = 0;
my $d = 9**9**9;

while(3*$c-2 <= $d) {
    my $p = getp(++$c);
    for (1..$c) {
        my $tp = getp($_);
        $d = $p-$tp if (isp($p-$tp) and isp($p+$tp) and $p-$tp < $d);
    }
}
say $d;

sub getp {
    my $n = shift;
    return $n*(3*$n-1)/2;
}

sub isp {
    my $p = shift;
    return (((1+sqrt(1+24*$p))/6)=~ m/^\d+$/);
}