#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::BigInt;

my $c = 0;
my ($k, $K) = (Math::BigInt->new(1), Math::BigInt->new(2));
my ($o, $t) = (Math::BigInt->new(1), Math::BigInt->new(2));
for (1..1000) {
    ($k, $K) = dv(($o, $o), sm(($t, $o), ($k, $K)));
    my ($n, $N) = sm(($o, $o), ($k, $K));
    $c++ if (length($n->bstr()) > length($N->bstr()));
}

say $c;

sub sm {
    my($a, $A, $b, $B) = @_;
    return ((Math::BigInt->new($a)->bmul(Math::BigInt->new($B)))->badd(Math::BigInt->new($b)->bmul(Math::BigInt->new($A))), Math::BigInt->new($A)->bmul(Math::BigInt->new($B)))
}

sub dv{
    my($a, $A, $b, $B) = @_;
    return ((Math::BigInt->new($a)->bmul(Math::BigInt->new($B))), (Math::BigInt->new($b)->bmul(Math::BigInt->new($A))))
}