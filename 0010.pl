#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my $r = 0;
my @p = @{primes(2000000)};
foreach (@p) {
	$r += $_;
}

say $r;