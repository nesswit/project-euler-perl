#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $r = 0;
foreach (1..999) {
	if ($_%3 == 0 or $_%5 == 0) {
		$r += $_;
	}
}

say $r;