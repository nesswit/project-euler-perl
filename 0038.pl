#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $r = 0;
for my $i (1..9999) {
	my $s = "";
	for (my $j = 1; 1; $j++) {
		if (length($s) < 9) {
			$s .= $i*$j;
		} elsif (length($s) == 9) {
			if ("123456789" eq join("", sort split(//, $s))) { 
				if ($r < $s) {
					$r = $s;
				}
			}
			last;
		} else {
			last;
		}
	}
}

say $r;