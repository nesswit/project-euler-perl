#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Permute::Array;
use Math::Prime::Util ':all';

my $r = 0;
for (1..7) {
	my @a = (1..$_);
	my $p = new Math::Permute::Array(\@a);
	for my $i (0..$p->cardinal()-1) {
		my $s = join("", @{$p->permutation($i)});
		$r = $s if ($r < $s and is_prime($s));
	}
}

say $r;