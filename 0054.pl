#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Games::Cards::Poker;

#명령행 인자로 파일 이름 넘겨줄 것.

my $c = 0;
while (<>) {
    my @a = split(/ /, $_);
    $c++ if (ScoreHand(@a[0..4]) < ScoreHand(@a[5..9]));
}
say $c;