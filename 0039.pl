#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $r = 0;
my $c = 0;

for my $g (3..1000) {
	my $t = 0;
	for my $i (1..$g/3) {
		for my $j (1..($g-$i)/2) {
			my @a = sort {$a <=> $b} ($i ,$j, $g-$i-$j);
			$t++ if ($a[2] <= $a[1] + $a[0] and $a[2]**2 == $a[1]**2 + $a[0]**2);
		}
	}
	if ($t > $c) {
		$c = $t;
		$r = $g;
	}
}

say $r;