#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';
use List::PowerSet 'powerset_lazy';

my $n = 0;
while($n = next_prime($n)) {
    my @a = split(//, $n);
    my $p = powerset_lazy(0..$#a);
    while(my $set = $p->()) {
        my $c = 0;
        next if ($#{$set} == -1 or $#{$set} == $#a);
        for my $i (0..9) {
            next if ($i == 0 and ${$set}[0] == 0);
            my @t = @a;
            for (@{$set}) {
                $t[$_] = $i;
            }
            $c++ if (is_prime(join("", @t)));
        }

        if ($c >= 8) {
            for my $i (0..9) {
                next if ($i == 0 and ${$set}[0] == 0);
                my @t = @a;
                for (@{$set}) {
                    $t[$_] = $i;
                }
                say join("", @t) if (is_prime(join("", @t)));
                exit;
            }
        }
    }
}
