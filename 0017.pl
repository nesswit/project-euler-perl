#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Number::Spell;

my $r = 0;
for (1..1000) {
        my $s = spell_number($_);
        $s =~ s/\s+//g;
        $r += length($s);
        $r += 3 if ($_ > 100 && $_ % 100 != 0);
}

say $r;