#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my @p = @{primes(1000, 9999)};

for my $i (@{primes(1000, 9999)}) {
    for my $j (@{primes($i+1, ($i+9999)/2)}) {
        my $k = $j*2-$i;
        if (is_prime($k) and f($i) eq f($j) and f($j) eq f($k)) {
            say $i.$j.$k;
        }
    }
}

sub f {
    return join("", sort split (//, shift));
}