#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Permute::Array;

my @a = (1..9);
my $r = 0;
my %h;
my $p = new Math::Permute::Array(\@a);
for my $i (0..$p->cardinal()-1) {
	my @t = @{$p->permutation($i)};
	for my $i (0..7) {
		for my $j (0..7-$i) {
            no warnings;
			if (join("", @t[0..$i])*join("", @t[$i+1..$j]) == join("", @t[$j+1..8])) {
				$h{join("", @t[$j+1..8])} = 1;
			}
		}
	}
}

$r += $_ foreach (keys %h);
say $r;