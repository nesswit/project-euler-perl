#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $n = 0;
while (++$n) {
    for (2..6) {
        last if (join("", sort split(//, $n)) ne join("", sort split(//, $n*$_)));
        if ($_ == 6) {
            say $n;
            exit;
        }
    }
}