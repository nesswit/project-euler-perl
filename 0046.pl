#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my $n = 3;
while ($n += 2) {
    next if is_prime($n);
    # say $n;
    my $a = 1;
    my $b = 0;
    while ($n-2*$a**2>1) {
        if (is_prime($n-2*$a**2)) {
            $b = 1;
            last;
        }
        $a++;
    }
    if (!$b) {
        last;
    }
}

say $n;