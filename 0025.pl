#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use bigint;
use Math::NumSeq::Fibonacci;

my $seq = Math::NumSeq::Fibonacci->new ();
while (1) {
	(my $i, my $value) = $seq->next();
	if (length($value) >= 1000) {
		say $i;
		exit;
	}
}