#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my @a;
for my $i (0..20) {
	for my $j (0..20) {
		if ($i == 0 or $j ==0) {
			$a[$i][$j] = 1;
		} else {
			$a[$i][$j] = $a[$i-1][$j] + $a[$i][$j-1];
		}
	}
}

say $a[20][20];