#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $a = 0;
my $b = 0;
foreach (1..100) {
	$b += $_**2;
	$a += $_;
}

say $a**2-$b;