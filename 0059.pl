#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

#명령행 인자로 파일 이름 넘겨줄 것.

my @a;
my @p;
my $r = 0;
@a = split /,/, $_ while (<>);
push @{$p[$_%3]}, $a[$_] for (0..$#a);
for (0..2) {
    my @u = @{$p[$_]};
    my $k = k(@{$p[$_]});
    for (0..$#u) {
        $u[$_] = $k^$u[$_];
        $r += $u[$_];
    }
    @{$p[$_]} = @u;
}

# for (0..$#a) {
#     $a[$_] = chr($p[$_%3]->[(int $_/3)]);
# }
# say @a;
say $r;

sub k {
    my %h;
    for (@_) {
        $h{$_} = 1 if not defined $h{$_};
        $h{$_}++ if defined $h{$_};
    }
    
    my $t = 0;
    my $v;
    for (keys %h) {
        if ($h{$_} > $t) {
            $t = $h{$_};
            $v = $_;
        }
    }
    
    return $v^ord(" ");
}