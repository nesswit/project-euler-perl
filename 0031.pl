#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $r = 0;
for my $h (-1..200) {
	next if ($h == -1);
for my $g (-1..max(0, 200 - (200*$h))) {
	next if ($g == -1);
for my $f (-1..max(0, 200 - (100*$g + 200*$h))) {
	next if ($f == -1);
for my $e (-1..max(0, 200 - (40*$f + 100*$g + 200*$h))) {
	next if ($e == -1);
for my $d (-1..max(0, 200 - (20*$e + 40*$f + 100*$g + 200*$h))) {
	next if ($d == -1);
for my $c (-1..max(0, 200 - (10*$d + 20*$e + 40*$f + 100*$g + 200*$h))) {
	next if ($c == -1);
for my $b (-1..max(0, 200 - (4*$c + 10*$d + 20*$e + 40*$f + 100*$g + 200*$h))) {
	next if ($b == -1);
for my $a (-1..max(0, 200 - (2*$b + 4*$c + 10*$d + 20*$e + 40*$f + 100*$g + 200*$h))) {
	next if ($a == -1);
	$r++ if (200 == 1*$a + 2*$b + 5*$c + 10*$d + 20*$e + 50*$f + 100*$g + 200*$h);
}}}}}}}} # sorry...!

say $r;

sub max {
	return $_[0] > $_[1] ? $_[0] : $_[1];
}