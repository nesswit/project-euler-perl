#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use bigint;

my $r = 0;
my @a = split(//, 2**1000);
for (@a) {
	$r += $_;
}

say $r;