#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::BigInt;

my $c = 0;
for (1..10000) {
    my $n = Math::BigInt->new($_); 
    for (1..50) {
        $n->badd(Math::BigInt->new(scalar reverse $n->bstr()));
        last if ($n == Math::BigInt->new(scalar reverse $n->bstr()));
        $c++ if ($_ == 50);
    }
}

say $c;