#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my $r = 0;
my @o = (0..28122);
my @a;

for (1..28123) {
	my $t = 0;
	$t += $_ for (all_factors($_));
	push (@a, $_) if ($_ < $t+1);
}

for my $i (@a) {
	for my $j (@a) {
		$o[$i+$j] = undef;
	}
}
for (@o) {
    $r += $_ if defined $_;
}

say $r;