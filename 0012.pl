#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my $j = 0;
my $i = 1;
while (1) {
	$j += $i++;
	my @p = all_factors($j); # without $j
	if ($#p >= 500) {
		say $j;
		exit;
	}
}