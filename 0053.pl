#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::NumSeq::Factorials;

my $s = Math::NumSeq::Factorials->new;
my $c = 0;
for my $n (1..100) {
    for my $r (1..$n) {
        $c++ if (1000000 < $s->ith($n)/(($s->ith($r)*($s->ith($n-$r)))));
    }
}
say $c;