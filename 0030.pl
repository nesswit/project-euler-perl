#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $r = 0;
my $l = 9;
$l .= 9 while ($l < length($l)*(9**5));

for my $i (2..$l) {
	my $a = 0;
	$a += $_**5 foreach (split (//, $i));
	$r += $i if ($i == $a);
}

say $r;
