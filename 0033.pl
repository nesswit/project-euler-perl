#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $c = 1;
my $p = 1;
for my $i (1..9) {
	for my $j (0..9) {
		my $n = (9*$i*$j)/(10*$i-$j);
		my $a = (10*$i+$n);
		my $b = (10*$n+$j);
		if ($i != $n and $a > 9 and $a < 100 and $b > 9 and $b < 100 and isint($n)) {
			$c *= $a;
			$p *= $b;
		}
	}
}

say $p/gcd($c, $p);

sub isint{
  my $val = shift;
  return ($val =~ m/^\d+$/);
}

sub gcd {
  my $a = $_[0];
  my $b = $_[1];

  while ($b != 0) {
    my $temp = $a % $b;
    $a = $b;
    $b = $temp;
  }

  return abs($a);
}