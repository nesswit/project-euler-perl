#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my $n = 1;
my $c = 1;
my ($p, $q) = (0, 1);
while (1) {
    for (1..4) {
        $n += 2*$c;
        if (is_prime($n)) {
            $p++;
        } else {
            $q++;
        }
    }
    if ($p/($p+$q) < 0.1) {
        say 2*$c+1;
        exit;
    }
    $c++;
}