#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::Prime::Util ':all';

my @p = @{primes(1000000)};
my $n = 0;
my $d = 0;

for my $i (0..$#p) {
    my $t = 0;
    for my $j (0..$#p) {
        last if not defined $p[$i+$j];
        $t += $p[$i+$j];
        last if ($t > 1000000); 
        if (is_prime($t) and $d < $j) {
            $n = $t;
            $d = $j;
        }
    }
}

say $n;