#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;
use Math::BigInt;

my %h;
for my $i (2..100) {
	for my $j (2..100) {
		$h{Math::BigInt->new($i)->bpow($j)} = 1;
	}
}
say scalar keys %h;
