#!/usr/bin/env perl
use 5.010;
use strict;
use warnings;

my $r = 1;
for my $i (1..500) {
	for my $j (0..3) {
		$r+=((2*$i+1)**2-2*$i*$j);
	}
}

say $r;

# #!perl
# my @a;
# my $x = 500;
# my $y = 500;
# my $p = 0;
# my $c = 1;
# my $r = 0;
# while (true) {
# 	$a[$x][$y] = $c++;
# 	last if ($x == 1000 and $y == 0);
# 	if ($p % 4 == 0) {
# 		$x++;
# 		$p++ if ($a[$x][$y+1] == undef);
# 	} elsif ($p % 4 == 1) {
# 		$y++;
# 		$p++ if ($a[$x-1][$y] == undef);
# 	} elsif ($p % 4 == 2) {
# 		$x--;
# 		$p++ if ($a[$x][$y-1] == undef);
# 	} elsif ($p % 4 == 3) {
# 		$y--;
# 		$p++ if ($a[$x+1][$y] == undef);
# 	}
# }

# foreach (0..1000) {
# 	$r += $a[$_][$_] + $a[$_][1000-$_];
# }
# print $r - 1;